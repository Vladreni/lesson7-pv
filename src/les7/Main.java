package les7;

import java.util.*;
import static java.lang.System.exit;

//калькулятор на алгоритме обратной польской нотации
public class Main {
    public static void main(String[] args) {
        String input = "(16-(0-4))/(12+3)+2".replace( " ", "");

        StringBuilder str = new StringBuilder();
        char[] chars = input.toCharArray();
        for (char c : chars) {          //разделяем все числа и знаки пробелами
            if (c == '/' || c == '*' || c == '+' || c == '-' || c == '(' || c == ')') {
                str.append(' ');
                str.append(c);
                str.append(' ');
            } else {
                str.append(c);
            }
        }
        //убираем сдвоенные пробелы и пробелы в начале и в конце
        str = new StringBuilder(str.toString().replace("  ", " ").replaceAll("^\\s+|\\s+$", ""));
        System.out.println(str);

        String[] parseStr = str.toString().split("[\\s]"); //разделяем строку в массив с разделителем пробел

        Stack<String> stackSymbol = new Stack<>();  //промежуточный стек только символов
        ArrayList<String> polsk = new ArrayList<>(); // для хранения польской обратной записи

        for (String value : parseStr) {   //алгоритм создания обратной польской нотации (запись)
            if (value.equals("(")) {        //действия при нахождении открывающей скобки
                stackSymbol.push(value);

            } else if (value.equals(")")) { //действия при нахождении закрывающей скобки
                boolean f = false;
                while (!stackSymbol.isEmpty() && !stackSymbol.peek().matches("[(]")) {
                    polsk.add(stackSymbol.pop());
                    f = true;
                }
                stackSymbol.pop();
                if (!f) {           //если ни разу не вошли в цикл значит знаков в скобках нет
                    System.out.println("Выражение в скобках не содержит знак");
                    exit(0);
                }
            } else if (value.equals("+") || value.equals("-")) {    //действия при нахождении + или -
                while (!stackSymbol.isEmpty() && stackSymbol.peek().matches("[-/+*]")) {
                    polsk.add(stackSymbol.pop());
                }
                stackSymbol.push(value);

            } else if (value.equals("/") || value.equals("*")) {    //действия при нахождении / или *
                if (!stackSymbol.isEmpty() && stackSymbol.peek().matches("[/*]")) {
                    polsk.add(stackSymbol.pop());
                }
                stackSymbol.push(value);

            } else {                //действия при нахождении цифр
                polsk.add(value);
            }
        }
        while (!stackSymbol.isEmpty()) {        //выталкиваем оставшиеся знаки в запись
            polsk.add(stackSymbol.pop());
        }

        Stack<String>  stack = new Stack<>();
        //перебираем элементы записи
        for (String s : polsk) {
            if (s.matches("[-/+*]")) {

                str = new StringBuilder();
                try {                               //берем знак и два операнда перед ним
                    str.append(stack.pop()).append(s).append(stack.pop());
                } catch (Exception e) {               //если стек пуст - операндов не хватает
                    System.out.println("В выражении не хватает операндов / два знака подряд");
                    exit(0);
                }
                //отправляем простое выражение в дз шестого урока, результат в стек
                stack.push(Double.toString(simple(str.toString())));
            } else {
                stack.push(s);                      //если у нас числа - добавляем в стек
            }
        }

        System.out.println(stack.pop());            //выводим результат на экран
        if (!stack.isEmpty())
            System.out.println("Выражение неверное");
    }
    //ДЗ шестого урока, добавлено распознавание плавающих точек
    public static double simple(String input) {
        if (input.matches("^[-]*[0-9]*[.,]?[0-9]++[-+*/]+[0-9]*[.,]?[0-9]+$")) {
            if (input.contains("+")){
                String [] arr = input.split("\\+");
                return (Double.parseDouble(arr[1]) + Double.parseDouble(arr[0]));
            }
            else if (input.contains("-")){
                String [] arr = input.split( "-(?!.*-)"); //только по последнему минусу если их два
                return (Double.parseDouble(arr[1]) - Double.parseDouble(arr[0]));
            }
            else if (input.contains("*")){
                String [] arr = input.split("\\*");
                return (Double.parseDouble(arr[1]) * Double.parseDouble(arr[0]));
            }
            else if (input.contains("/")){
                String [] arr = input.split("\\/");
                if (Double.parseDouble( arr[0]) == 0) {
                    System.out.println("Деление на ноль");
                    exit(0);
                }
                else
                    return (Double.parseDouble(arr[1]) / Double.parseDouble(arr[0]));
            }
        }
        return 0;
    }
}


